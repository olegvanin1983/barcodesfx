package site.barsukov.barcodefx.model;

public class KM {
    private String gtin;
    private String serial;
    private String suzString;
    private boolean sscc = false;

    public KM() {
    }

    public KM(String gtin, String serial) {
        this.gtin = gtin;
        this.serial = serial;
    }

    public String getGtin() {
        return gtin;
    }

    public String getShortGtin() {
        return gtin.substring(2);
    }

    public String getShortSscc() {
        if (!sscc) {
            throw new IllegalArgumentException("Код не является SSCC");
        }
        return suzString.substring(2);
    }

    public void setGtin(String gtin) {
        this.gtin = gtin;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getSuzString() {
        return suzString;
    }

    public void setSuzString(String suzString) {
        this.suzString = suzString;
    }

    public boolean isSscc() {
        return sscc;
    }

    public void setSscc(boolean sscc) {
        this.sscc = sscc;
    }

    @Override
    public String toString() {
        return "KM{" +
            "gtin='" + gtin + '\'' +
            ", serial='" + serial + '\'' +
            ", suzString='" + suzString + '\'' +
            ", sscc=" + sscc +
            '}';
    }
}
