package site.barsukov.barcodefx.model.enums;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum GoodCategory {
    SHOES("Обувь", 13, 88),
    ATP("АТП", 7, 0),
    TIRES("Шины", 13, 44),
    CLOTHES("Одежда", 13, 44),
    MILK("Молоко", 13, 0);

    private static Map<String, GoodCategory> names = Stream.of(GoodCategory.values())
        .collect(Collectors.toMap(GoodCategory::getShortName, s -> s));

    private String shortName;
    private int serialLength;
    private int tailLength;


    GoodCategory(String shortName, int serialLength, int tailLength) {
        this.shortName = shortName;
        this.serialLength = serialLength;
        this.tailLength = tailLength;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public static GoodCategory getByName(String name) {
        return names.get(name);
    }

    public int getSerialLength() {
        return serialLength;
    }

    public void setSerialLength(int serialLength) {
        this.serialLength = serialLength;
    }

    public int getTailLength() {
        return tailLength;
    }

    public void setTailLength(int tailLength) {
        this.tailLength = tailLength;
    }

    @Override
    public String toString() {
        return shortName;
    }
}
