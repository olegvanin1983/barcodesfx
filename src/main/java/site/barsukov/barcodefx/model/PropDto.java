package site.barsukov.barcodefx.model;

public class PropDto {
    private String id;
    private String descr;
    private String value;

    public PropDto(String id, String descr, String value) {
        this.id = id;
        this.descr = descr;
        this.value = value;

    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
