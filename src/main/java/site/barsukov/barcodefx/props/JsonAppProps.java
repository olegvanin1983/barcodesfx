package site.barsukov.barcodefx.props;

public class JsonAppProps {
    private JsonCatalogProps jsonCatalogProps;
    private JsonPrintProps jsonPrintProps;
    private JsonSystemProps jsonSystemProps;
    private JsonSsccProps jsonSsccProps;
    private JsonUpdateProps jsonUpdateProps;
    private JsonLastStateProps jsonLastStateProps;

    public JsonCatalogProps getJsonCatalogProps() {
        return jsonCatalogProps;
    }

    public void setJsonCatalogProps(JsonCatalogProps jsonCatalogProps) {
        this.jsonCatalogProps = jsonCatalogProps;
    }

    public JsonPrintProps getJsonPrintProps() {
        return jsonPrintProps;
    }

    public void setJsonPrintProps(JsonPrintProps jsonPrintProps) {
        this.jsonPrintProps = jsonPrintProps;
    }

    public JsonSsccProps getJsonSsccProps() {
        return jsonSsccProps;
    }

    public void setJsonSsccProps(JsonSsccProps jsonSsccProps) {
        this.jsonSsccProps = jsonSsccProps;
    }

    public JsonUpdateProps getJsonUpdateProps() {
        return jsonUpdateProps;
    }

    public void setJsonUpdateProps(JsonUpdateProps jsonUpdateProps) {
        this.jsonUpdateProps = jsonUpdateProps;
    }

    public JsonLastStateProps getJsonLastStateProps() {
        return jsonLastStateProps;
    }

    public void setJsonLastStateProps(JsonLastStateProps jsonLastStateProps) {
        this.jsonLastStateProps = jsonLastStateProps;
    }

    public JsonSystemProps getJsonSystemProps() {
        return jsonSystemProps;
    }

    public void setJsonSystemProps(JsonSystemProps jsonSystemProps) {
        this.jsonSystemProps = jsonSystemProps;
    }
}
