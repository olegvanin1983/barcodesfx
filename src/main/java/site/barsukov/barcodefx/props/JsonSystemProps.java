package site.barsukov.barcodefx.props;

public class JsonSystemProps extends JsonBaseProp {
    public enum SystemProp implements IProp {
        NEWS_WINDOW_HEIGHT("Высота окна с новостями", "782"),
        NEWS_WINDOW_WIDTH("Ширина окна с новостями", "1277"),
        SERVERS_PARAMS("Параметры подключения к серверам", "");

        private String descr;
        private String defaultValue;

        SystemProp(String descr, String defaultValue) {
            this.descr = descr;
            this.defaultValue = defaultValue;
        }

        public String getDescr() {
            return descr;
        }

        public String getDefaultValue() {
            return defaultValue;
        }
    }

}
