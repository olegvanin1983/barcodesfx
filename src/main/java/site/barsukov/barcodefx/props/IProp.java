package site.barsukov.barcodefx.props;

//@JsonSerialize(as=IProp.class)
public interface IProp {
    String getDescr();

    String getDefaultValue();

    String name();
}
