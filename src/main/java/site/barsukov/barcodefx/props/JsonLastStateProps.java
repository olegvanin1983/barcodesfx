package site.barsukov.barcodefx.props;

public class JsonLastStateProps extends  JsonBaseProp{
    public enum LastStateProp implements IProp {

        CATEGORY("Последняя выбранная категория", "SHOES"),
        LABEL_HEIGHT("Высота этикетки", "113"),
        LABEL_WIDTH("Ширина этикетки", "164"),
        LABEL_TEXT("Текст на этикетке", ""),
        LABEL_START_NUMBER("Начинать нумерацию с", "1"),
        LABEL_NUMERATE("Нумеровать", "false"),
        LABEL_SIZE_A4("Этикетка А4", "true"),
        LABEL_SIZE_TERMOPRINTER("Этикетка термопринтера", "false"),
        LABEL_HORIZONTAL("Этикетка горизонтальная", "true"),
        LABEL_VERTICAL("Этикетка вертикальная", "false");
        private String descr;
        private String defaultValue;

        LastStateProp(String descr, String defaultValue) {
            this.descr = descr;
            this.defaultValue = defaultValue;
        }

        public String getDescr() {
            return descr;
        }

        public String getDefaultValue() {
            return defaultValue;
        }
    }

}
