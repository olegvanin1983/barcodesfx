package site.barsukov.barcodefx.services;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.stax2.XMLOutputFactory2;
import site.barsukov.barcodefx.CrptEscapeFactory;
import site.barsukov.barcodefx.Validator;
import site.barsukov.barcodefx.context.ProductionContext;
import site.barsukov.barcodefx.model.KM;
import site.barsukov.barcodefx.model.crpt.productionrf.CertificateTypeType;
import site.barsukov.barcodefx.model.crpt.productionrf.ProductionOrderType;
import site.barsukov.barcodefx.model.crpt.productionrf.Vvod;
import site.barsukov.barcodefx.serializer.CrptProductProductionRFSerializer;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class XMLProductionService extends BaseDocService<ProductionContext> {
    private static final int ACTION_ID = 5;
    private static final int VERSION_ID = 5;
    static final Logger logger = Logger.getLogger(XMLProductionService.class);

    public XMLProductionService(ProductionContext context) {
        super(context);
    }

    public File performAction() throws IOException, JAXBException {
        File csvFile = new File(context.getCsvFileName());
        String fileName = FilenameUtils.removeExtension(csvFile.getName());
        File xmlFile = new File(context.getResultFolder() + File.separatorChar + fileName + getRangeString() + "_production_rf.xml");

        createProductionRF(xmlFile, csvFile);

        return xmlFile;
    }

    private void createProductionRF(File xmlFile, File csvFile) throws IOException, JAXBException {
        validateINNs(context);
        validateTnved(context);
        List<KM> KMs = readKMsFromFile(csvFile, context);

        Vvod result = new Vvod();
        result.setTradeParticipantInn(context.getParticipantInn());
        result.setProducerInn(context.getProducerInn());
        result.setOwnerInn(context.getOwnerInn());
        result.setProductDate(context.getProductDate());
        if (context.getProductionOrder() != null) {
            result.setProductionOrder(ProductionOrderType.valueOf(context.getProductionOrder()));
        }

        Vvod.ProductsList list = new Vvod.ProductsList();
        list.getProduct().addAll(KMs.stream()
            .map(s -> XMLProductionService.createProductionProduct(s, context))
            .collect(Collectors.toList()));
        result.setProductsList(list);
        result.setActionId(ACTION_ID);
        result.setVersion(VERSION_ID);

        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.getFactory().getXMLOutputFactory().setProperty(XMLOutputFactory2.P_TEXT_ESCAPER,
            CrptEscapeFactory.theInstance);   //for escaping <>&'" symbols
        xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
        xmlMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        JaxbAnnotationModule module = new JaxbAnnotationModule();
        SimpleModule customSerializer = new SimpleModule();
        customSerializer.addSerializer(Vvod.ProductsList.class, new CrptProductProductionRFSerializer());
        xmlMapper.registerModule(customSerializer);
        xmlMapper.registerModule(module);

        String xml = xmlMapper.writeValueAsString(result);

        FileUtils.writeStringToFile(xmlFile, xml, "UTF-8");
    }

    private void validateTnved(ProductionContext context) {
        if (StringUtils.isBlank(context.getTnvedCode())) {
            throw new IllegalArgumentException("Не заполнен ТН ВЭД");
        }
        if (context.getTnvedCode().length() != 10) {
            throw new IllegalArgumentException("ТН ВЭД должен быть десятизначный");
        }
    }

    private void validateINNs(ProductionContext context) {
        Validator.validateINN(context.getOwnerInn());
        Validator.validateINN(context.getParticipantInn());
        Validator.validateINN(context.getProducerInn());
    }

    private static Vvod.ProductsList.Product createProductionProduct(KM km, ProductionContext context) {
        Vvod.ProductsList.Product result = new Vvod.ProductsList.Product();
        if (km.isSscc()) {
            result.setKitu(km.getShortSscc());
        } else {
            result.setKit(km.getGtin() + km.getSerial());
        }
        result.setProductDate(context.getProductDate());
        result.setTnvedCode(context.getTnvedCode());
        if (context.getCertificateType() != null) {
            result.setCertificateType(CertificateTypeType.valueOf(context.getCertificateType()));
        }
        result.setCertificateNumber(StringUtils.isBlank(context.getCertificateNumber()) ? null : context.getCertificateNumber());
        result.setCertificateDate(context.getCertificateDate());

        return result;
    }

    @Override
    public String getServiceName() {
        return "XML_PRODUCTION_SERVICE";
    }

    @Override
    public boolean isXmlResult() {
        return true;
    }
}
