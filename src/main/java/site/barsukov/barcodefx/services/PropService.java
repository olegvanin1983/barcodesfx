package site.barsukov.barcodefx.services;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import site.barsukov.barcodefx.model.PickingServer;
import site.barsukov.barcodefx.props.*;
import site.barsukov.barcodefx.serializer.IPropKeyDeserializer;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

import static site.barsukov.barcodefx.props.JsonSystemProps.SystemProp.SERVERS_PARAMS;
import static site.barsukov.barcodefx.props.JsonUpdateProps.UpdateProp.*;

public class PropService {
    static final Logger logger = Logger.getLogger(PropService.class);
    private static final String PATH = "properties.json";
    private ObjectMapper mapper = new ObjectMapper();
    private static volatile PropService instance;
    private static volatile String UNKNOWN = "UNKNOWN";
    private String sessionToken;

    private PropService() {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        SimpleModule simpleKeyModule = new SimpleModule();
        simpleKeyModule.addKeyDeserializer(IProp.class, new IPropKeyDeserializer());
        mapper.registerModule(simpleKeyModule);
        sessionToken = UUID.randomUUID().toString();
    }

    public static PropService getInstance() {
        PropService result = instance;
        if (result != null) {
            return result;
        }
        synchronized (PropService.class) {
            if (instance == null) {
                instance = new PropService();
            }
            return instance;
        }
    }

    public JsonAppProps getProps() {
        return getProps(new File(PATH));
    }

    public List<PickingServer> getServers() throws IOException, GeneralSecurityException {
        JsonAppProps props = getProps();
        String serverProps = props.getJsonSystemProps().getProp(SERVERS_PARAMS);
        if (StringUtils.isBlank(serverProps)) {
            return new ArrayList<>();
        }
        return decryptServerPasswords(mapper.readValue(serverProps, new TypeReference<List<PickingServer>>() {
        }), props.getJsonUpdateProps().getProp(CLIENT_ID).toCharArray());
    }


    public void saveServers(List<PickingServer> servers) throws JsonProcessingException, GeneralSecurityException, UnsupportedEncodingException {
        JsonAppProps props = getProps();
        encryptServerPasswords(servers, props.getJsonUpdateProps().getProp(CLIENT_ID).toCharArray());
        props.getJsonSystemProps().setProp(SERVERS_PARAMS, mapper.writeValueAsString(servers));
        saveProps(props);
    }


    public synchronized JsonAppProps getProps(File file) {
        JsonAppProps result = null;
        try {
            String propsline = FileUtils.readFileToString(file, "UTF-8");
            result = mapper.readValue(propsline, JsonAppProps.class);
        } catch (Exception e) {
            logger.error("Error reading props", e);
            result = new JsonAppProps();
        }

        return fillDefaults(result);
    }

    private JsonAppProps fillDefaults(JsonAppProps result) {
        boolean rewrite = false;
        if (result.getJsonCatalogProps() == null) {
            result.setJsonCatalogProps(new JsonCatalogProps());
        }
        if (result.getJsonPrintProps() == null) {
            result.setJsonPrintProps(new JsonPrintProps());
        }
        if (result.getJsonSsccProps() == null) {
            result.setJsonSsccProps(new JsonSsccProps());
        }
        if (result.getJsonUpdateProps() == null) {
            result.setJsonUpdateProps(new JsonUpdateProps());
        }
        if (result.getJsonLastStateProps() == null) {
            result.setJsonLastStateProps(new JsonLastStateProps());
        }
        if (result.getJsonSystemProps() == null) {
            result.setJsonSystemProps(new JsonSystemProps());
        }
        if (!result.getJsonUpdateProps().getProp(APP_VERSION).equals(getClass().getPackage().getImplementationVersion())) {
            result.getJsonUpdateProps().setProp(APP_VERSION, getClass().getPackage().getImplementationVersion());
            rewrite = true;
        }
        if (UNKNOWN.equals(result.getJsonUpdateProps().getProp(CLIENT_ID))) {
            result.getJsonUpdateProps().setProp(CLIENT_ID, UUID.randomUUID().toString());
            rewrite = true;
        }

        if (!sessionToken.equals(result.getJsonUpdateProps().getProp(SESSION_ID))) {
            result.getJsonUpdateProps().setProp(SESSION_ID, sessionToken);
            rewrite = true;
        }

        if (rewrite) {
            saveProps(result);
        }
        return result;
    }

    public void saveProps(JsonAppProps props) {
        saveProps(props, new File(PATH));
    }

    public void saveProps(JsonAppProps props, File file) {
        try {
            mapper.writeValue(file, props);
        } catch (IOException e) {
            logger.error("Ошибка сохранения настроек", e);
        }

    }

    private static SecretKeySpec createSecretKey(char[] password) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeySpecException, NoSuchAlgorithmException {
        byte[] salt = new String("12345678").getBytes();

        // Decreasing this speeds down startup time and can be useful during testing, but it also makes it easier for brute force attackers
        int iterationCount = 40000;
        // Other values give me java.security.InvalidKeyException: Illegal key size or default parameters
        int keyLength = 128;

        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
        PBEKeySpec keySpec = new PBEKeySpec(password, salt, iterationCount, keyLength);
        SecretKey keyTmp = keyFactory.generateSecret(keySpec);
        return new SecretKeySpec(keyTmp.getEncoded(), "AES");
    }

    private static String encrypt(String property, SecretKeySpec key) throws GeneralSecurityException, UnsupportedEncodingException {
        Cipher pbeCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        pbeCipher.init(Cipher.ENCRYPT_MODE, key);
        AlgorithmParameters parameters = pbeCipher.getParameters();
        IvParameterSpec ivParameterSpec = parameters.getParameterSpec(IvParameterSpec.class);
        byte[] cryptoText = pbeCipher.doFinal(property.getBytes("UTF-8"));
        byte[] iv = ivParameterSpec.getIV();
        return base64Encode(iv) + ":" + base64Encode(cryptoText);
    }

    private static String encrypt(String property, char[] password) throws GeneralSecurityException, UnsupportedEncodingException {
        return encrypt(property, createSecretKey(password));
    }

    private static String base64Encode(byte[] bytes) {
        return Base64.getEncoder().encodeToString(bytes);
    }

    private static String decrypt(String string, SecretKeySpec key) throws GeneralSecurityException, IOException {
        String iv = string.split(":")[0];
        String property = string.split(":")[1];
        Cipher pbeCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        pbeCipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(base64Decode(iv)));
        return new String(pbeCipher.doFinal(base64Decode(property)), "UTF-8");
    }

    private static String decrypt(String string, char[] password) throws GeneralSecurityException, IOException {
        return decrypt(string, createSecretKey(password));
    }

    private static byte[] base64Decode(String property) throws IOException {
        return Base64.getDecoder().decode(property);
    }

    private List<PickingServer> decryptServerPasswords(List<PickingServer> readValue, char[] password) throws GeneralSecurityException, IOException {
        for (PickingServer server : readValue) {
            if (StringUtils.isNotBlank(server.getPassword())) {
                server.setPassword(decrypt(server.getPassword(), password));
            }
        }
        return readValue;
    }

    private void encryptServerPasswords(List<PickingServer> servers, char[] password) throws GeneralSecurityException, UnsupportedEncodingException {
        for (PickingServer server : servers) {
            if (StringUtils.isNotBlank(server.getPassword())) {
                server.setPassword(encrypt(server.getPassword(), password));
            }
        }
    }
}
