package site.barsukov.barcodefx.services;

import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import org.apache.log4j.Logger;
import site.barsukov.barcodefx.model.VersionAnswerDto;
import site.barsukov.barcodefx.props.JsonAppProps;

import java.awt.*;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static site.barsukov.barcodefx.props.JsonUpdateProps.UpdateProp.CHECK_UPDATES;
import static site.barsukov.barcodefx.props.JsonUpdateProps.UpdateProp.IGNORABLE_VERSIONS;

public class UpdateService extends Service {
    static final Logger logger = Logger.getLogger(UpdateService.class);
    private WebService webService;
    private PropService propService = PropService.getInstance();
    private boolean updateEnabled = propService.getProps().getJsonUpdateProps().getBooleanProp(CHECK_UPDATES);


    public UpdateService(WebService webService) {
        this.webService = webService;
    }

    @Override
    protected Task createTask() {
        return new Task() {
            @Override
            protected Object call() throws Exception {
                try {
                    Thread.sleep(15000);
                } catch (InterruptedException e) {
                    logger.error("Unexpected interruption ", e);
                }
                Platform.runLater(() -> {
                    if (updateEnabled) {
                        checkVersion();
                    }
                });
                return null;
            }
        };
    }

    private void checkVersion() {
        String curVersion = getClass().getPackage().getImplementationVersion();
        VersionAnswerDto answer = webService.getLastVersionInfo();
        Set<String> ignorableVersions = getIgnorableVersions();
        if (answer != null
            && !answer.getVersion().equals(curVersion)
            && !ignorableVersions.contains(answer.getVersion())) {

            ButtonType ignoreButton = new ButtonType("Пропустить версию", ButtonBar.ButtonData.NEXT_FORWARD);
            ButtonType okButton = new ButtonType("Да", ButtonBar.ButtonData.OK_DONE);
            ButtonType cancelButton = new ButtonType("Нет", ButtonBar.ButtonData.CANCEL_CLOSE);
            Alert confirmWindow = new Alert(
                Alert.AlertType.CONFIRMATION,
                "Скачать новую версию?", ignoreButton, cancelButton, okButton);

            confirmWindow.setHeaderText(String.format("Доступна новая версия программы - %s.", answer.getVersion()));
            confirmWindow.setTitle("Проверка обновлений");
            Optional<ButtonType> result = confirmWindow.showAndWait();
            if (result.isPresent() && result.get() == okButton) {
                try {
                    Desktop.getDesktop().browse(new URI(answer.getUrl()));
                } catch (Exception e) {
                    logger.error("Error while opening update page: ", e);
                }
            } else if (result.isPresent() && result.get() == ignoreButton) {
                ignorableVersions.add(answer.getVersion());
                saveIgnorableVersions(ignorableVersions);
            }

        }
    }

    private Set<String> getIgnorableVersions() {
        String ignorableString = propService.getProps().getJsonUpdateProps().getProp(IGNORABLE_VERSIONS);
        return new HashSet(Arrays.asList(ignorableString.split(";")));
    }

    private synchronized void saveIgnorableVersions(Set<String> ignorableVersions) {
        JsonAppProps props = propService.getProps();
        props.getJsonUpdateProps().setProp(IGNORABLE_VERSIONS, String.join(";", ignorableVersions));
        propService.saveProps(props);
    }
}
