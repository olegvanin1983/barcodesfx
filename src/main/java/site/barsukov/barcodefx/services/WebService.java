package site.barsukov.barcodefx.services;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import site.barsukov.barcodefx.model.JsonSFAnswer;
import site.barsukov.barcodefx.model.LogDTO;
import site.barsukov.barcodefx.model.VersionAnswerDto;
import site.barsukov.barcodefx.model.enums.GoodCategory;
import site.barsukov.barcodefx.props.JsonAppProps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static site.barsukov.barcodefx.props.JsonUpdateProps.UpdateProp.*;

public class WebService {
    static final Logger logger = Logger.getLogger(WebService.class);
    private ObjectMapper mapper = new ObjectMapper();

    public WebService() {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static void logAction(String serviceName, long linesProcessed, JsonAppProps jsonAppProps, GoodCategory category) {
        try {
            if (jsonAppProps.getJsonUpdateProps().getBooleanProp(STATS_ENABLED)) {
                LogDTO logDTO = new LogDTO();
                String clientId = jsonAppProps.getJsonUpdateProps().getProp(CLIENT_ID);
                logDTO.setClientId(clientId);
                logDTO.setSessionId(jsonAppProps.getJsonUpdateProps().getProp(SESSION_ID));
                logDTO.setAppVersion(jsonAppProps.getJsonUpdateProps().getProp(APP_VERSION));
                logDTO.setLinesProcessed(linesProcessed);
                logDTO.setServiceName(serviceName);
                if (category != null) {
                    logDTO.setCategoryName(category.name());
                }


                String statUrl = jsonAppProps.getJsonUpdateProps().getProp(STATS_URL);
                HttpPost post = new HttpPost(statUrl + "/barcodesfx/stat");

                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                mapper.enable(SerializationFeature.INDENT_OUTPUT);

                String jsonString = mapper.writeValueAsString(logDTO);
                post.setEntity(new StringEntity(jsonString));

                post.setHeader("Accept", "application/json");
                post.setHeader("Content-type", "application/json; charset=utf-8");
                post.setHeader("Barcodes-token", clientId);


                try (CloseableHttpClient httpClient = HttpClientBuilder.create()
                    .build();
                     CloseableHttpResponse response = httpClient.execute(post)) {
                } catch (Exception e) {
                    logger.error("Ошибка отправки статистики " + e);
                }
            }
        } catch (Exception e) {
            logger.error("Ошибка отправки статистики " + e);
        }
    }


    public VersionAnswerDto getLastVersionInfo() {
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet("https://sourceforge.net/projects/barcodesfx/best_release.json");
        try {
            logger.info(request.toString());
            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            line = rd.readLine();
            JsonSFAnswer answer = mapper.readValue(line, JsonSFAnswer.class);
            VersionAnswerDto release = answer.getPlatformRelease().getWindows();
            release.setVersion(getVersion(release.getFilename()));
            return release;
        } catch (IOException e) {
            logger.error("Ошибка получения обновления");
            return null;
        }
    }

    private String getVersion(String filename) {
        filename = FilenameUtils.removeExtension(filename);
        return filename.replace("/barCodesFX-", "");
    }

}
