package site.barsukov.barcodefx.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import site.barsukov.barcodefx.model.crpt.vvodimportfts.VvodImportFts;

import java.io.IOException;

public class CrptProductVvodImportFtsSerializer extends StdSerializer<VvodImportFts.ProductsList> {

    public CrptProductVvodImportFtsSerializer() {
        this(null);
    }

    public CrptProductVvodImportFtsSerializer(Class<VvodImportFts.ProductsList> t) {
        super(t);
    }

    @Override
    public void serialize(VvodImportFts.ProductsList value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        for (VvodImportFts.ProductsList.Product curProduct : value.getProduct()) {
            gen.writeObjectField("product", curProduct);
        }
        gen.writeEndObject();
    }
}
