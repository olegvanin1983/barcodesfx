package site.barsukov.barcodefx.context;

public class CancellationContext extends BaseContext {
    private String participantINN;
    private String reason;
    private String cancellationDate;
    private String cancellationDocNum;

    public String getParticipantINN() {
        return participantINN;
    }

    public void setParticipantINN(String participantINN) {
        this.participantINN = participantINN;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getCancellationDate() {
        return cancellationDate;
    }

    public void setCancellationDate(String cancellationDate) {
        this.cancellationDate = cancellationDate;
    }

    public String getCancellationDocNum() {
        return cancellationDocNum;
    }

    public void setCancellationDocNum(String cancellationDocNum) {
        this.cancellationDocNum = cancellationDocNum;
    }
}
