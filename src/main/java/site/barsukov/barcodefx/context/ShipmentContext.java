package site.barsukov.barcodefx.context;

public class ShipmentContext extends BaseContext {
    private String receiverINN;
    private String senderINN;
    private String transferDate;
    private String moveDocDate;
    private String moveDocNum;
    private String shipmentType;
    private String withdrawType;
    private String withdrawDate;
    private boolean shippingToParticipant;


    public String getReceiverINN() {
        return receiverINN;
    }

    public void setReceiverINN(String receiverINN) {
        this.receiverINN = receiverINN;
    }

    public String getSenderINN() {
        return senderINN;
    }

    public void setSenderINN(String senderINN) {
        this.senderINN = senderINN;
    }

    public String getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(String transferDate) {
        this.transferDate = transferDate;
    }

    public String getMoveDocDate() {
        return moveDocDate;
    }

    public void setMoveDocDate(String moveDocDate) {
        this.moveDocDate = moveDocDate;
    }

    public String getMoveDocNum() {
        return moveDocNum;
    }

    public void setMoveDocNum(String moveDocNum) {
        this.moveDocNum = moveDocNum;
    }

    public String getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public boolean isShippingToParticipant() {
        return shippingToParticipant;
    }

    public void setShippingToParticipant(boolean shippingToParticipant) {
        this.shippingToParticipant = shippingToParticipant;
    }

    public String getWithdrawType() {
        return withdrawType;
    }

    public void setWithdrawType(String withdrawType) {
        this.withdrawType = withdrawType;
    }

    public String getWithdrawDate() {
        return withdrawDate;
    }

    public void setWithdrawDate(String withdrawDate) {
        this.withdrawDate = withdrawDate;
    }
}
