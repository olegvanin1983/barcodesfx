package site.barsukov.barcodefx.context;

import java.time.LocalDate;

public class AcceptanceContext extends BaseContext {
    private String receiverINN;
    private String senderINN;
    private String transferDate;
    private LocalDate transferDateDate;
    private String moveDocDate;
    private String moveDocNum;
    private String acceptanceType;
    private String shipmentId;

    public String getReceiverINN() {
        return receiverINN;
    }

    public void setReceiverINN(String receiverINN) {
        this.receiverINN = receiverINN;
    }

    public String getSenderINN() {
        return senderINN;
    }

    public void setSenderINN(String senderINN) {
        this.senderINN = senderINN;
    }

    public String getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(String transferDate) {
        this.transferDate = transferDate;
    }

    public String getMoveDocDate() {
        return moveDocDate;
    }

    public void setMoveDocDate(String moveDocDate) {
        this.moveDocDate = moveDocDate;
    }

    public String getMoveDocNum() {
        return moveDocNum;
    }

    public void setMoveDocNum(String moveDocNum) {
        this.moveDocNum = moveDocNum;
    }

    public String getAcceptanceType() {
        return acceptanceType;
    }

    public void setAcceptanceType(String acceptanceType) {
        this.acceptanceType = acceptanceType;
    }

    public LocalDate getTransferDateDate() {
        return transferDateDate;
    }

    public void setTransferDateDate(LocalDate transferDateDate) {
        this.transferDateDate = transferDateDate;
    }

    public String getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }
}
