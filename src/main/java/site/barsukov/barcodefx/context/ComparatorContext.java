package site.barsukov.barcodefx.context;

import java.io.File;

public class ComparatorContext extends BaseContext {
    private boolean ignoreTails;
    private File comparedFile;

    public File getComparedFile() {
        return comparedFile;
    }

    public void setComparedFile(File comparedFile) {
        this.comparedFile = comparedFile;
    }

    public boolean isIgnoreTails() {
        return ignoreTails;
    }

    public void setIgnoreTails(boolean ignoreTails) {
        this.ignoreTails = ignoreTails;
    }
}
