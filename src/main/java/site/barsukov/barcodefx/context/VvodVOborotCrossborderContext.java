package site.barsukov.barcodefx.context;

import java.time.LocalDate;

public class VvodVOborotCrossborderContext extends BaseContext {
    private String userINN;
    private String senderINN;
    private String exporterName;
    private String countryOksm;
    private LocalDate importDate;
    private String primaryDocNum;
    private LocalDate primaryDocDate;

    public String getUserINN() {
        return userINN;
    }

    public void setUserINN(String userINN) {
        this.userINN = userINN;
    }

    public String getSenderINN() {
        return senderINN;
    }

    public void setSenderINN(String senderINN) {
        this.senderINN = senderINN;
    }

    public String getExporterName() {
        return exporterName;
    }

    public void setExporterName(String exporterName) {
        this.exporterName = exporterName;
    }

    public String getCountryOksm() {
        return countryOksm;
    }

    public void setCountryOksm(String countryOksm) {
        this.countryOksm = countryOksm;
    }


    public String getPrimaryDocNum() {
        return primaryDocNum;
    }

    public void setPrimaryDocNum(String primaryDocNum) {
        this.primaryDocNum = primaryDocNum;
    }


    public LocalDate getImportDate() {
        return importDate;
    }

    public void setImportDate(LocalDate importDate) {
        this.importDate = importDate;
    }

    public LocalDate getPrimaryDocDate() {
        return primaryDocDate;
    }

    public void setPrimaryDocDate(LocalDate primaryDocDate) {
        this.primaryDocDate = primaryDocDate;
    }
}
