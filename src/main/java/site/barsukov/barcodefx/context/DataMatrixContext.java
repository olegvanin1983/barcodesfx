package site.barsukov.barcodefx.context;

public class DataMatrixContext extends BaseContext {
    private boolean vertical;
    private boolean horizontal;
    private boolean termoPrinter;
    private String height;
    private String templateName;
    private String width;
    private String userLabelText;
    private Boolean printMarkNumber;
    private int markNumberStart;
    private int totalPageNumber;


    public boolean isVertical() {
        return vertical;
    }

    public void setVertical(boolean vertical) {
        this.vertical = vertical;
    }

    public boolean isHorizontal() {
        return horizontal;
    }

    public void setHorizontal(boolean horizontal) {
        this.horizontal = horizontal;
    }

    public boolean isTermoPrinter() {
        return termoPrinter;
    }

    public void setTermoPrinter(boolean termoPrinter) {
        this.termoPrinter = termoPrinter;
    }

    public float getHeight() {
        try {
            return Float.parseFloat(height.trim());
        } catch (Exception e) {
            throw new IllegalArgumentException("Ошибка определения высоты этикетки.");
        }
    }

    public void setHeight(String height) {
        this.height = height;
    }


    public float getWidth() {
        try {
            return Float.parseFloat(width.trim());
        } catch (Exception e) {
            throw new IllegalArgumentException("Ошибка определения ширины этикетки.");
        }
    }

    public void setWidth(String width) {
        this.width = width;
    }


    public String getUserLabelText() {
        return userLabelText;
    }

    public void setUserLabelText(String userLabelText) {
        this.userLabelText = userLabelText;
    }

    public Boolean getPrintMarkNumber() {
        return printMarkNumber;
    }

    public void setPrintMarkNumber(Boolean printMarkNumber) {
        this.printMarkNumber = printMarkNumber;
    }

    public int getMarkNumberStart() {
        return markNumberStart;
    }

    public void setMarkNumberStart(int markNumberStart) {
        this.markNumberStart = markNumberStart;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public int getTotalPageNumber() {
        return totalPageNumber;
    }

    public void setTotalPageNumber(int totalPageNumber) {
        this.totalPageNumber = totalPageNumber;
    }
}
