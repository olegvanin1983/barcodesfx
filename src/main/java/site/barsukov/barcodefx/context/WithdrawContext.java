package site.barsukov.barcodefx.context;

import java.time.LocalDate;

public class WithdrawContext extends BaseContext {
    private String kktNumber;
    private String primaryDocumentCustomName;
    private LocalDate primaryDocumentDate;
    private String primaryDocumentNumber;
    private String primaryDocumentType;
    private String tradeParticipantInn;
    private String withdrawalType;
    private LocalDate withdrawalDate;
//    private List<WithdrawalProduct> products = new ArrayList<>();

    public String getKktNumber() {
        return kktNumber;
    }

    public void setKktNumber(String kktNumber) {
        this.kktNumber = kktNumber;
    }

    public String getPrimaryDocumentCustomName() {
        return primaryDocumentCustomName;
    }

    public void setPrimaryDocumentCustomName(String primaryDocumentCustomName) {
        this.primaryDocumentCustomName = primaryDocumentCustomName;
    }

    public LocalDate getPrimaryDocumentDate() {
        return primaryDocumentDate;
    }

    public void setPrimaryDocumentDate(LocalDate primaryDocumentDate) {
        this.primaryDocumentDate = primaryDocumentDate;
    }

    public String getPrimaryDocumentNumber() {
        return primaryDocumentNumber;
    }

    public void setPrimaryDocumentNumber(String primaryDocumentNumber) {
        this.primaryDocumentNumber = primaryDocumentNumber;
    }

    public String getPrimaryDocumentType() {
        return primaryDocumentType;
    }

    public void setPrimaryDocumentType(String primaryDocumentType) {
        this.primaryDocumentType = primaryDocumentType;
    }

    public String getTradeParticipantInn() {
        return tradeParticipantInn;
    }

    public void setTradeParticipantInn(String tradeParticipantInn) {
        this.tradeParticipantInn = tradeParticipantInn;
    }

    public LocalDate getWithdrawalDate() {
        return withdrawalDate;
    }

    public void setWithdrawalDate(LocalDate withdrawalDate) {
        this.withdrawalDate = withdrawalDate;
    }

    public String getWithdrawalType() {
        return withdrawalType;
    }

    public void setWithdrawalType(String withdrawalType) {
        this.withdrawalType = withdrawalType;
    }

//    public List<WithdrawalProduct> getProducts() {
//        return products;
//    }
//
//    public void setProducts(List<WithdrawalProduct> products) {
//        this.products = products;
//    }
}
