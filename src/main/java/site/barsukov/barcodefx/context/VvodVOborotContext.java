package site.barsukov.barcodefx.context;

public class VvodVOborotContext extends BaseContext {
    private String userINN;

    public String getUserINN() {
        return userINN;
    }

    public void setUserINN(String userINN) {
        this.userINN = userINN;
    }
}
