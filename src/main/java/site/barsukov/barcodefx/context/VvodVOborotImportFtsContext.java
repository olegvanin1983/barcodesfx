package site.barsukov.barcodefx.context;

import java.time.LocalDate;

public class VvodVOborotImportFtsContext extends BaseContext {
    private String userINN;
    private String declarationNum;
    private LocalDate declarationDate;

    public String getUserINN() {
        return userINN;
    }

    public void setUserINN(String userINN) {
        this.userINN = userINN;
    }

    public String getDeclarationNum() {
        return declarationNum;
    }

    public void setDeclarationNum(String declarationNum) {
        this.declarationNum = declarationNum;
    }

    public LocalDate getDeclarationDate() {
        return declarationDate;
    }

    public void setDeclarationDate(LocalDate declarationDate) {
        this.declarationDate = declarationDate;
    }
}
