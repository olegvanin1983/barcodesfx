package site.barsukov.barcodefx.context;

public class AggregationContext extends BaseContext {
    private String documentNumber;
    private String participantINN;
    private String orgName;
    private String sscc;
    private boolean ip; //является ли ИП-шником

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getParticipantINN() {
        return participantINN;
    }

    public void setParticipantINN(String participantINN) {
        this.participantINN = participantINN;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getSscc() {
        return sscc;
    }

    public void setSscc(String ssc) {
        this.sscc = ssc;
    }

    public boolean isIp() {
        return ip;
    }

    public void setIp(boolean ip) {
        this.ip = ip;
    }
}
