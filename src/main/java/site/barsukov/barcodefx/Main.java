package site.barsukov.barcodefx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import site.barsukov.barcodefx.controllers.AboutController;
import site.barsukov.barcodefx.controllers.Controller;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/sample.fxml"));
        Parent root = fxmlLoader.load();
        primaryStage.setTitle("BarCodesFX-" + getClass().getPackage().getImplementationVersion());
        primaryStage.setScene(new Scene(root));
        Controller controller = fxmlLoader.getController();
        primaryStage.setOnCloseRequest(
            event -> controller.onCloseActions()
        );
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);

    }

}
