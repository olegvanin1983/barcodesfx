package site.barsukov.barcodefx;

import com.itextpdf.barcodes.Barcode128;
import com.itextpdf.barcodes.BarcodeDataMatrix;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.VerticalAlignment;
import org.apache.commons.lang3.StringUtils;
import site.barsukov.barcodefx.context.DataMatrixContext;
import site.barsukov.barcodefx.model.KM;
import site.barsukov.barcodefx.props.JsonPrintProps;

import java.io.IOException;

import static com.itextpdf.barcodes.Barcode128.FNC1;
import static com.itextpdf.barcodes.BarcodeDataMatrix.*;
import static site.barsukov.barcodefx.props.JsonPrintProps.PrintProp.*;

public class CreateMarkTableUtil {
    public static Table createMarkTableVertical(KM km, PdfDocument document, float height, float width, DataMatrixContext context, Integer number) throws IOException {
        JsonPrintProps printProps = context.getJsonAppProps().getJsonPrintProps();
        Table result = new Table(1);

        PdfFont f1 = PdfFontFactory.createFont("TimesNewRoman.ttf",
            "CP1251", true);
        result.addCell(createDMImage(km, document, context.getJsonAppProps().getJsonPrintProps()));
        if (km.isSscc()) {
            return result;
        }
        result.startNewRow();

        Cell cell = new Cell();

        Paragraph phrase = new Paragraph();
        if (!StringUtils.isBlank(context.getUserLabelText())) {
            phrase.add(new Text(context.getUserLabelText() + "\n")
                .setFont(f1)
                .setFontSize(printProps.getFloatProp(USER_LABEL_FONT_SIZE))
                .setBold());
        }


        phrase.add(new Text(Utils.selectAI(km.getGtin()) + "\n" + Utils.selectAI(km.getSerial()) + "\n")
            .setFont(f1)
            .setFontSize(printProps.getFloatProp(LABEL_FONT_SIZE)));


        if (context.getPrintMarkNumber()) {
            phrase.add(printProps.getProp(LABEL_COUNTER_PREFIX) + number)
                .setFont(f1)
                .setFontSize(printProps.getFloatProp(LABEL_FONT_SIZE));

        }
        phrase.setHorizontalAlignment(HorizontalAlignment.CENTER);
        phrase.setVerticalAlignment(VerticalAlignment.MIDDLE);
        phrase.setMarginLeft(printProps.getFloatProp(LABEL_A4_TEXT_MARGIN_LEFT));

        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setHorizontalAlignment(HorizontalAlignment.LEFT);
        cell.add(phrase);
        cell.setWidth(width);
        float imageCellHeight = printProps.getFloatProp(DATAMATRIX_IMAGE_HEIGHT) + printProps.getFloatProp(DATAMATRIX_IMAGE_MARGIN_TOP) + printProps.getFloatProp(DATAMATRIX_IMAGE_MARGIN_BOTTOM);
        cell.setHeight(height - imageCellHeight);
        result.addCell(cell);
        return result;

    }


    public static Table createMarkTableHorizontal(KM km, PdfDocument document, float height, float width, DataMatrixContext context, Integer number) throws IOException {
        JsonPrintProps printProps = context.getJsonAppProps().getJsonPrintProps();
        Float labelFontSize = printProps.getFloatProp(LABEL_FONT_SIZE);

        Table result = new Table(2);

        PdfFont f1 = PdfFontFactory.createFont("TimesNewRoman.ttf",
            "CP1251", true);

        Cell cell = new Cell();
        Paragraph phrase = new Paragraph();
        if (!StringUtils.isBlank(context.getUserLabelText())) {
            phrase.add(new Text(context.getUserLabelText() + "\n")
                .setFont(f1)
                .setFontSize(printProps.getFloatProp(USER_LABEL_FONT_SIZE))
                .setBold());
        }
        if (km.isSscc()) {
            phrase.add(new Text(Utils.selectAI(km.getSuzString()))
                .setFont(f1).setFontSize(labelFontSize));
        } else {
            phrase.add(new Text(Utils.selectAI(km.getGtin()) + "\n" + Utils.selectAI(km.getSerial()) + "\n")
                .setFont(f1).setFontSize(labelFontSize));
        }

        if (context.getPrintMarkNumber()) {
            phrase.add(printProps.getProp(LABEL_COUNTER_PREFIX) + number)
                .setFont(f1).setFontSize(labelFontSize);

        }
        phrase.setHorizontalAlignment(HorizontalAlignment.LEFT);
        phrase.setVerticalAlignment(VerticalAlignment.MIDDLE);
        phrase.setMarginLeft(printProps.getFloatProp(LABEL_A4_TEXT_MARGIN_LEFT));

        cell.add(phrase);
        float imageCellWidth = printProps.getFloatProp(DATAMATRIX_IMAGE_WIDTH) + printProps.getFloatProp(DATAMATRIX_IMAGE_MARGIN_LEFT) + printProps.getFloatProp(DATAMATRIX_IMAGE_MARGIN_RIGHT);
        cell.setWidth(width - imageCellWidth);
        cell.setHeight(height);
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);

        if (!km.isSscc()) {
            result.addCell(cell);
        }

        Cell imageCell = new Cell();
        imageCell.add(createDMImage(km, document, printProps));
        imageCell.setVerticalAlignment(VerticalAlignment.MIDDLE);

        result.addCell(imageCell);

        return result;

    }

    public static Image createDMImage(KM km, PdfDocument document, JsonPrintProps printProps) {
        return createDMImage(km, document, printProps, true);
    }

    public static Image createDMImage(KM km, PdfDocument document, JsonPrintProps printProps, boolean printFnc) {
        Image image;
        if (km.isSscc()) {
            image = new Image(createEANBarcodeObject(km.getSuzString(), document));
            image.setHeight(printProps.getFloatProp(EAN_IMAGE_HEIGHT));
            image.setWidth(printProps.getFloatProp(EAN_IMAGE_WIDTH));
            image.setHorizontalAlignment(HorizontalAlignment.CENTER);
            image.setMargins(
                printProps.getFloatProp(EAN_IMAGE_MARGIN_TOP),
                printProps.getFloatProp(EAN_IMAGE_MARGIN_RIGHT),
                printProps.getFloatProp(EAN_IMAGE_MARGIN_BOTTOM),
                printProps.getFloatProp(EAN_IMAGE_MARGIN_LEFT)
            );
        } else {
            image = new Image(createDMBarcodeObject(km.getSuzString(), document, printFnc));
            image.setHeight(printProps.getFloatProp(DATAMATRIX_IMAGE_HEIGHT));
            image.setWidth(printProps.getFloatProp(DATAMATRIX_IMAGE_WIDTH));
            image.setHorizontalAlignment(HorizontalAlignment.CENTER);
            image.setMargins(
                printProps.getFloatProp(DATAMATRIX_IMAGE_MARGIN_TOP),
                printProps.getFloatProp(DATAMATRIX_IMAGE_MARGIN_RIGHT),
                printProps.getFloatProp(DATAMATRIX_IMAGE_MARGIN_BOTTOM),
                printProps.getFloatProp(DATAMATRIX_IMAGE_MARGIN_LEFT)
            );
        }


        return image;
    }


    private static PdfFormXObject createDMBarcodeObject(String dataString, PdfDocument document, boolean printFnc) {
        BarcodeDataMatrix dataMatrix = new BarcodeDataMatrix();
        if (printFnc) {
            dataMatrix.setOptions(DM_EXTENSION + DM_TEXT);
            dataMatrix.setCode("f." + dataString);
        } else {
            dataMatrix.setOptions(DM_TEXT);
            dataMatrix.setCode(dataString);
        }

        dataMatrix.setOptions(DM_AUTO);
        return dataMatrix.createFormXObject(null, 1, document);
    }

    private static PdfFormXObject createEANBarcodeObject(String dataString, PdfDocument document) {
        Barcode128 dataMatrix = new Barcode128(document);
        dataMatrix.setAltText(Utils.selectAI(dataString));
        dataMatrix.setCode(FNC1 + dataString);
        return dataMatrix.createFormXObject(null, null, document);
    }
}
