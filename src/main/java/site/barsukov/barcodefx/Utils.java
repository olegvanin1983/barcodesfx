package site.barsukov.barcodefx;

import javafx.scene.control.DatePicker;
import org.apache.commons.lang3.StringUtils;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.GregorianCalendar;

public class Utils {
    public static final String TEMPLATES_FOLDER_PATH = "templates";

    public static String selectAI(String source) {
        StringBuilder builder = new StringBuilder();
        builder.append('(');
        builder.append(source.substring(0, 2));
        builder.append(')');
        builder.append(source.substring(2));
        return builder.toString();
    }

    public static String getStringDate(DatePicker datePicker, DateTimeFormatter dateTimeFormatter) {
        if (datePicker.getValue() != null) {
            return datePicker.getValue().format(dateTimeFormatter);
        } else if (StringUtils.isNotBlank(datePicker.getEditor().getText())) {
            dateTimeFormatter.parse(datePicker.getEditor().getText());
            return datePicker.getEditor().getText();
        } else {
            return null;
        }
    }

    public static LocalDate getLocalDate(DatePicker datePicker, DateTimeFormatter dateTimeFormatter) {
        if (datePicker.getValue() != null) {
            return datePicker.getValue();
        } else {
            if (StringUtils.isBlank(datePicker.getEditor().getText())) {
                return null;
            }
            return LocalDate.parse(datePicker.getEditor().getText(), dateTimeFormatter);
        }
    }

    public static LocalDate getLocalDate(DatePicker datePicker) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        return getLocalDate(datePicker, dateTimeFormatter);
    }

    public static void checkArgument(boolean condition, String errorMessage) {
        if (!condition) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    public static XMLGregorianCalendar getXMLGregorianCalendarNow()
        throws DatatypeConfigurationException {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
        XMLGregorianCalendar now =
            datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
        return now;
    }

}
