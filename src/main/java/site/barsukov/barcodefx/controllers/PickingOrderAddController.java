package site.barsukov.barcodefx.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import site.barsukov.barcodefx.model.enums.GoodCategory;
import site.barsukov.barcodefx.model.yo.pickingorder.PickingOrder;
import site.barsukov.barcodefx.services.PickingOrderService;
import site.barsukov.barcodefx.services.PropService;
import site.barsukov.barcodefx.services.WebService;

import javax.xml.datatype.DatatypeConfigurationException;
import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ResourceBundle;

import static site.barsukov.barcodefx.Utils.getXMLGregorianCalendarNow;
import static site.barsukov.barcodefx.services.PickingOrderService.convertTg;

public class PickingOrderAddController implements Initializable {
    final static Logger logger = Logger.getLogger(PickingOrderAddController.class);
    public TextField fileNameTf;
    public TextField statusTf;
    public TextField commentTf;
    public ComboBox<GoodCategory> goodCategoryCb;
    private File sourceFile;
    private GoodCategory selectedGoodCategory;
    private PickingOrder pickingOrder;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        goodCategoryCb.getItems().addAll(GoodCategory.values());
        goodCategoryCb.getSelectionModel().select(0);
    }

    public void setFile(File file) {
        this.sourceFile = file;
        fileNameTf.setText(FilenameUtils.getName(sourceFile.getAbsolutePath()));
    }

    public void setPickingOrder(PickingOrder pickingOrder) {
        this.pickingOrder = pickingOrder;
        if (pickingOrder == null) {
            return;
        }
        commentTf.setText(pickingOrder.getComment());
        statusTf.setText(pickingOrder.getState());
        statusTf.setText(pickingOrder.getState());
        goodCategoryCb.getSelectionModel().select(PickingOrderService.convertTg(pickingOrder.getProductGroup()));
    }

    public void setGoodCategory(GoodCategory goodCategory) {
        this.selectedGoodCategory = goodCategory;
        goodCategoryCb.getSelectionModel().select(selectedGoodCategory);
    }

    public void okButtonAction(ActionEvent actionEvent) {
        Stage stage = (Stage) fileNameTf.getScene().getWindow();
        try {
            File destFile = PickingOrderService.savePickingOrder(sourceFile, getPickingOrder());
            statusTf.getScene().setUserData(destFile);
        } catch (Exception e) {
            logger.error("Error while adding picking order ", e);
            new Alert(Alert.AlertType.ERROR, "Ошибка при добавлении пикинг ордера " + e.getMessage()).showAndWait();
        } finally {
            new Thread(() -> {
                WebService.logAction("ADD_PICKING_ORDER",  0,
                    PropService.getInstance().getProps(), goodCategoryCb.getSelectionModel().getSelectedItem());
            }).start();
            stage.close();
        }

    }

    private PickingOrder getPickingOrder() throws DatatypeConfigurationException {
        if (pickingOrder == null) {
            pickingOrder = new PickingOrder();
            pickingOrder.setFileName(fileNameTf.getText());
            pickingOrder.setState(statusTf.getText());
            pickingOrder.setComment(commentTf.getText());
//        pickingOrder.setSscc();
//        pickingOrder.setCodesType(getType(lines, category));
            pickingOrder.setCreationDate(getXMLGregorianCalendarNow());
            pickingOrder.setModificationDate(getXMLGregorianCalendarNow());
//        pickingOrder.setSecurity();
            pickingOrder.setProductGroup(convertTg(goodCategoryCb.getSelectionModel().getSelectedItem()));
            pickingOrder.setVersion(BigDecimal.ONE);
        } else {
            pickingOrder.setState(statusTf.getText());
            pickingOrder.setComment(commentTf.getText());
            pickingOrder.setProductGroup(convertTg(goodCategoryCb.getSelectionModel().getSelectedItem()));
            pickingOrder.setModificationDate(getXMLGregorianCalendarNow());
        }

        return pickingOrder;
    }

    public void cancelButtonAction(ActionEvent actionEvent) {
        Stage stage = (Stage) fileNameTf.getScene().getWindow();
        stage.close();
    }
}
