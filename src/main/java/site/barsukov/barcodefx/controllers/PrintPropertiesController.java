package site.barsukov.barcodefx.controllers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import site.barsukov.barcodefx.model.PropDto;
import site.barsukov.barcodefx.props.JsonAppProps;
import site.barsukov.barcodefx.props.JsonPrintProps;
import site.barsukov.barcodefx.services.PropService;

import java.net.URL;
import java.util.ResourceBundle;


public class PrintPropertiesController implements Initializable {
    static final Logger logger = Logger.getLogger(PrintPropertiesController.class);
    public TableView<PropDto> propList;
    public JsonAppProps allProps;
    private PropService propService = PropService.getInstance();


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        allProps = propService.getProps();

        TableColumn<PropDto, String> propName = new TableColumn<PropDto, String>("Название");
        propName.setCellValueFactory(new PropertyValueFactory<PropDto, String>("id"));
        propList.getColumns().add(propName);

        TableColumn<PropDto, String> propDescr = new TableColumn<PropDto, String>("Описание");
        propDescr.setCellValueFactory(new PropertyValueFactory<PropDto, String>("descr"));
        propList.getColumns().add(propDescr);

        TableColumn<PropDto, String> propValue = new TableColumn<PropDto, String>("Значение");
        propValue.setCellValueFactory(new PropertyValueFactory<PropDto, String>("value"));
        propValue.setEditable(true);
        propValue.setCellFactory(TextFieldTableCell.forTableColumn());
        propValue.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<PropDto, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<PropDto, String> event) {
                (event.getTableView().getItems().get(
                    event.getTablePosition().getRow())
                ).setValue(event.getNewValue());
            }
        });

        propList.getColumns().add(propValue);

        propList.setEditable(true);
    }


    public void okButtonAction(ActionEvent actionEvent) {
        JsonPrintProps printProps = allProps.getJsonPrintProps();
        for (PropDto curProp : propList.getItems()) {
            printProps.setProp(JsonPrintProps.PrintProp.valueOf(curProp.getId()), curProp.getValue());
        }
        allProps.setJsonPrintProps(printProps);
        propService.saveProps(allProps);
        Stage stage = (Stage) propList.getScene().getWindow();
        stage.close();
    }

    public void cancelButtonAction(ActionEvent actionEvent) {
        Stage stage = (Stage) propList.getScene().getWindow();
        stage.close();
    }


    public void filData() {
        JsonPrintProps printProps = allProps.getJsonPrintProps();
        for (JsonPrintProps.PrintProp curProp : JsonPrintProps.PrintProp.values()) {
            propList.getItems()
                .add(new PropDto(curProp.name(),
                    curProp.getDescr(),
                    printProps.getProp(curProp)));
        }
    }
}
