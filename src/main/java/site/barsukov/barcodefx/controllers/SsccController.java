package site.barsukov.barcodefx.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import site.barsukov.barcodefx.props.JsonAppProps;
import site.barsukov.barcodefx.props.JsonSsccProps;
import site.barsukov.barcodefx.services.PropService;
import site.barsukov.barcodefx.services.WebService;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static site.barsukov.barcodefx.props.JsonSsccProps.SsccProp.*;


public class SsccController implements Initializable {
    static final Logger logger = Logger.getLogger(SsccController.class);
    public TextField prefix;
    public TextField serialStart;
    public Label lastGeneratedSerialLabel;
    public TextField numberOfLabels;
    public ComboBox extension;
    public JsonAppProps allProps;
    private PropService propService = PropService.getInstance();


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        allProps = propService.getProps();
        prefix.setText(allProps.getJsonSsccProps().getProp(LAST_PREFIX));
        String lastSerialString = allProps.getJsonSsccProps().getProp(LAST_SERIAL);
        lastGeneratedSerialLabel.setText(lastSerialString);
        Integer lastSerial = Integer.parseInt(lastSerialString);
        serialStart.setText(Integer.toString(lastSerial + 1));
        numberOfLabels.setText("1");
        ArrayList<Integer> optionsList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            optionsList.add(i);
        }
        ObservableList<Integer> options = FXCollections.observableArrayList(optionsList);

        extension.setItems(options);
        extension.getSelectionModel().select(allProps.getJsonSsccProps().getFloatProp(LAST_EXTENSION).intValue());
    }


    public void generateButtonAction(ActionEvent actionEvent) throws IOException {
        try {
            generateSscc();
            new Thread(() -> {
                WebService.logAction("SSCC_GENERATED",
                    Long.parseLong(numberOfLabels.getText()),
                    PropService.getInstance().getProps(), null);
            }).start();
        } catch (IllegalArgumentException ex) {
            logger.error(ex);
            new Alert(Alert.AlertType.ERROR, ex.getMessage()).showAndWait();
        } catch (Exception e) {
            logger.error(e);
            new Alert(Alert.AlertType.ERROR, "Unexpected exception: " + e.getMessage()).showAndWait();
        }


    }

    private void generateSscc() throws IOException {

        // get a handle to the stage
        Stage stage = (Stage) lastGeneratedSerialLabel.getScene().getWindow();
        // do what you have to do
        FileChooser fileChooser = new FileChooser();

        //Set extension filter for text files
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showSaveDialog(stage);
        List<String> lines = generateLines();
        if (file != null) {
            FileUtils.writeLines(file, lines);
            updateProps();
            lastGeneratedSerialLabel.getScene().setUserData(file);
            stage.close();
        }
    }

    private void updateProps() {
        JsonSsccProps newSsccProps = new JsonSsccProps();
        newSsccProps.setProp(LAST_PREFIX, prefix.getText());
        Integer lastSerial = Integer.parseInt(serialStart.getText()) + Integer.parseInt(numberOfLabels.getText()) - 1;
        newSsccProps.setProp(LAST_SERIAL, Integer.toString(lastSerial));
        newSsccProps.setProp(LAST_EXTENSION,
            Integer.toString(extension.getSelectionModel().getSelectedIndex()));
        allProps.setJsonSsccProps(newSsccProps);
        propService.saveProps(allProps);
    }

    private List<String> generateLines() {
        List<String> result = new ArrayList<>();
        Integer start = Integer.parseInt(serialStart.getText());
        Integer end = start + Integer.parseInt(numberOfLabels.getText());
        for (int i = start; i < end; i++) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("00");
            String serial = StringUtils.leftPad(Integer.toString(i), 16 - prefix.getText().length(), '0');
            String sscc = extension.getSelectionModel().getSelectedItem() + prefix.getText() + serial;
            if (sscc.length() > 17) {
                throw new IllegalArgumentException("Длина SSCC превышает 17 символов " + sscc);
            }
            stringBuilder.append(sscc);
            stringBuilder.append(getControlDigit(sscc));
            result.add(stringBuilder.toString());
        }
        return result;
    }

    private String getControlDigit(String sscc) {
        int result = 0;
        int summ = 0;
        for (int i = 0; i < sscc.length(); i++) {
            int intChar = Character.getNumericValue(sscc.charAt(i));
            if (i % 2 != 0) {
                summ = summ + intChar;
            } else {
                summ = summ + intChar * 3;
            }
        }
        for (int i = 0; i < 10; i++) {
            if ((summ + i) % 10 == 0) {
                result = i;
            }
        }
        return Integer.toString(result);
    }

}
