package site.barsukov.barcodefx.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import site.barsukov.barcodefx.Validator;
import site.barsukov.barcodefx.services.PropService;
import site.barsukov.barcodefx.services.WebService;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;


public class XmlValidatorController implements Initializable {
    final static Logger logger = Logger.getLogger(XmlValidatorController.class);

    public TextArea texAreaAbout;
    public Label fileNameLabel;
    public Button validateFileButton;

    public void openHomePage(ActionEvent actionEvent) throws URISyntaxException, IOException {
        Desktop.getDesktop().browse(new URI("https://sourceforge.net/projects/barcodesfx/"));

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        validateFileButton.setDisable(true);
    }


    public void validateFileButtonAction(ActionEvent actionEvent) {
        texAreaAbout.clear();
        texAreaAbout.setText(Validator.validateXml(new File(fileNameLabel.getText())));
        new Thread(() -> {
            WebService.logAction("XML_VALIDATION", 0, PropService.getInstance().getProps(), null);
        }).start();
    }

    public void chooseFileButtonAction(ActionEvent actionEvent) {
        File csvFile;

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Выберите файл для валидации");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("xml", "*.xml"));
        csvFile = fileChooser.showOpenDialog(new Stage());


        if (csvFile != null) {
            fileNameLabel.setText(csvFile.getAbsolutePath());
            validateFileButton.setDisable(false);
        }
    }
}
