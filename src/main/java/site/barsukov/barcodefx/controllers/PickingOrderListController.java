package site.barsukov.barcodefx.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import site.barsukov.barcodefx.model.ArchivedPickingOrder;
import site.barsukov.barcodefx.model.PickingServer;
import site.barsukov.barcodefx.model.yo.pickingorder.PickingOrder;
import site.barsukov.barcodefx.services.PickingOrderService;
import site.barsukov.barcodefx.services.PropService;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import static site.barsukov.barcodefx.ListFiller.fillArchivedPickingOrdersTable;
import static site.barsukov.barcodefx.ListFiller.fillPickingOrdersTable;

public class PickingOrderListController implements Initializable {
    final static Logger logger = Logger.getLogger(PickingOrderListController.class);
    public TableView<PickingOrder> ordersTableView;
    public TableView<ArchivedPickingOrder> archiveTableView;
    public TableView<PickingOrder> remoteOrdersTableView;
    public ComboBox<PickingServer> remoteOrdersServersCB;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            fillPickingOrdersTable(ordersTableView);
            fillPickingOrdersTable(remoteOrdersTableView);
            fillArchivedPickingOrdersTable(archiveTableView);
            refreshLocalOrdersList();
            refreshLocalArchiveOrdersList();
            PickingOrderService.clearRemoteCache();
            refreshRemoteOrdersList();
            archiveTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            remoteOrdersServersCB.getItems().addAll(PropService.getInstance().getServers());
            if (!remoteOrdersServersCB.getItems().isEmpty()) {
                remoteOrdersServersCB.getSelectionModel().select(0);
            }
            remoteOrdersServersCB.getSelectionModel().selectedItemProperty().addListener(
                (options, oldValue, newValue) -> {
                    try {
                        PickingOrderService.clearRemoteCache();
                        remoteOrdersTableView.getItems().clear();
                    } catch (IOException e) {
                        logger.error("Error clearing remote cache: ", e);
                    }
                }
            );

            ordersTableView.setRowFactory(tv -> {
                TableRow<PickingOrder> row = new TableRow<>();
                row.setOnMouseClicked(event -> {
                    if (event.getClickCount() == 2 && (!row.isEmpty())) {
                        chooseOrder(row.getItem());
                    }
                });
                return row;
            });
        } catch (Exception e) {
            logger.error("Error initializing picking order view", e);
        }
    }


    public void chooseOrderButtonAction(ActionEvent actionEvent) {
        PickingOrder selectedItem = ordersTableView.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            new Alert(Alert.AlertType.INFORMATION, "Пикинг ордер не выбран").showAndWait();
        } else {
            chooseOrder(selectedItem);
        }
    }

    private void chooseOrder(PickingOrder selectedItem) {
        Stage stage = (Stage) ordersTableView.getScene().getWindow();
        File fileWithCodes = new File(PickingOrderService.LOCAL_PICKING_ORDERS_PATH + selectedItem.getFileName());
        ordersTableView.getScene().setUserData(fileWithCodes);
        stage.close();
    }

    public void addOrderFromFileButtonAction(ActionEvent actionEvent) {
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Выберите файл с кодами маркировки");
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("csv, txt", "*.csv", "*.txt"));

            File csvFile = fileChooser.showOpenDialog(new Stage());
            if (csvFile != null) {
                if (PickingOrderService.isOrderExist(csvFile)) {
                    Alert confirmWindow = new Alert(
                        Alert.AlertType.CONFIRMATION,
                        "Пикинг ордер с таким файлом кодов уже существует. Заменить?");
//                   confirmWindow.setHeaderText(String.format("Доступна новая версия программы - %s.", answer.getVersion()));
//                   confirmWindow.setTitle("Проверка обновлений");
                    Optional<ButtonType> result = confirmWindow.showAndWait();
                    if (result.isPresent() && result.get() == ButtonType.OK) {
                        openAddPickingOrderDialog(csvFile);
                    }
                } else {
                    openAddPickingOrderDialog(csvFile);
                }
            }
            refreshLocalOrdersList();
        } catch (Exception e) {
            logger.error("Error opening picking order list", e);
        }
    }

    public void sendToArchiveButtonAction(ActionEvent actionEvent) {
        try {
            PickingOrder selectedItem = ordersTableView.getSelectionModel().getSelectedItem();
            PickingOrderService.archivePickingOrder(selectedItem);
            refreshLocalOrdersList();
            refreshLocalArchiveOrdersList();
        } catch (IOException e) {
            logger.error("Error updating picking order meta info", e);
        }
    }

    public void editOrderButtonAction(ActionEvent actionEvent) {
        try {
            PickingOrder selectedItem = ordersTableView.getSelectionModel().getSelectedItem();
            if (selectedItem == null) {
                new Alert(Alert.AlertType.INFORMATION, "Пикинг ордер не выбран").showAndWait();
            } else {
                openAddPickingOrderDialog(
                    new File(PickingOrderService.LOCAL_PICKING_ORDERS_PATH + File.separatorChar + selectedItem.getFileName()),
                    selectedItem);
            }
            refreshLocalOrdersList();
        } catch (IOException e) {
            logger.error("Error updating picking order meta info", e);
        }
    }

    private File openAddPickingOrderDialog(File file) throws IOException {
        return openAddPickingOrderDialog(file, null);
    }


    private File openAddPickingOrderDialog(File file, PickingOrder pickingOrder) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/picking_order_add.fxml"));
        Parent root1 = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Добавление пикинг ордера");
        Scene scene = new Scene(root1);
        PickingOrderAddController controller = fxmlLoader.getController();
        controller.setFile(file);
        controller.setPickingOrder(pickingOrder);
//        controller.setGoodCategory(category.getSelectionModel().getSelectedItem());
        stage.setScene(scene);
        stage.showAndWait();
        return (File) scene.getUserData();
    }

    private void refreshLocalOrdersList() throws IOException {
        ordersTableView.getItems().clear();
        ordersTableView.getItems().addAll(PickingOrderService.getLocalPickingOrders());
    }

    private void refreshLocalArchiveOrdersList() {
        archiveTableView.getItems().clear();
        try {
            archiveTableView.getItems().addAll(PickingOrderService.getLocalArchivePickingOrders());
        } catch (IOException e) {
            logger.error("Error refreshing local archive list: ", e);
        }
    }

    private void refreshRemoteOrdersList() throws IOException {
        remoteOrdersTableView.getItems().clear();
        remoteOrdersTableView.getItems().addAll(PickingOrderService.getRemotePickingOrders());
    }

    public void sendToServerButtonAction(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/picking_server_chooser.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("Отправка пикинг ордеров");
            stage.setResizable(false);
            Scene scene = new Scene(root1);
            PickingServersChooserController controller = fxmlLoader.getController();
            controller.setOrders(ordersTableView.getSelectionModel().getSelectedItems());
            stage.setScene(scene);
            stage.showAndWait();
        } catch (Exception e) {
            logger.error("Error sending picking orders to server:", e);
        }
    }

    public void sendFromArchiveButtonAction(ActionEvent actionEvent) {
        try {
            List<ArchivedPickingOrder> selectedItems = archiveTableView.getSelectionModel().getSelectedItems();
            if (selectedItems != null
                && !selectedItems.isEmpty()
                && showConfirmationWindow(String.format("Количество записей для восстановления - %s. Восстановить?", selectedItems.size()))) {

                PickingOrderService.returnFromArchivePickingOrder(selectedItems);
                refreshLocalOrdersList();
                refreshLocalArchiveOrdersList();
            }
        } catch (IOException e) {
            logger.error("Error sendFromArchiveButtonAction ", e);
        }
    }

    public void deleteFromArchiveButtonAction(ActionEvent actionEvent) {
        List<ArchivedPickingOrder> selectedArchivedPickingOrders = archiveTableView.getSelectionModel().getSelectedItems();
        if (selectedArchivedPickingOrders != null
            && !selectedArchivedPickingOrders.isEmpty()
            && showConfirmationWindow(String.format("Количество записей для удаления - %s. Удалить?", selectedArchivedPickingOrders.size())
        )) {
            PickingOrderService.removeArchivedOrders(selectedArchivedPickingOrders);
        }
        refreshLocalArchiveOrdersList();
    }


    public void downloadRemoteOrderButtonAction(ActionEvent actionEvent) {
        List<PickingOrder> selectedRemotePickingOrders = remoteOrdersTableView.getSelectionModel().getSelectedItems();
        try {
            if (selectedRemotePickingOrders != null
                && !selectedRemotePickingOrders.isEmpty()
                && showConfirmationWindow(String.format("Количество записей для загрузки - %s. Загрузить?", selectedRemotePickingOrders.size())
            )) {
                PickingOrderService.downloadRemoteOrders(remoteOrdersServersCB.getSelectionModel().getSelectedItem(), selectedRemotePickingOrders);
            }
            refreshLocalOrdersList();
        } catch (IOException e) {
            logger.error("Error downloadRemoteOrderButtonAction: ", e);
        }
    }

    public void removeRemoteOrderButtonAction(ActionEvent actionEvent) {
        List<PickingOrder> selectedRemotePickingOrders = remoteOrdersTableView.getSelectionModel().getSelectedItems();
        try {
            if (selectedRemotePickingOrders != null
                && !selectedRemotePickingOrders.isEmpty()
                && showConfirmationWindow(String.format("Количество записей для удаления - %s. Удалить?", selectedRemotePickingOrders.size())
            )) {
                PickingOrderService.removeRemoteOrders(remoteOrdersServersCB.getSelectionModel().getSelectedItem(),
                    selectedRemotePickingOrders);
            }
        } catch (IOException e) {
            logger.error("Error removeRemoteOrderButtonAction:", e);
            new Alert(Alert.AlertType.ERROR, "Ошибка удаления файлов").showAndWait();
        }
        refreshRemoteCache();
    }

    private boolean showConfirmationWindow(String message) {
        Alert confirmWindow = new Alert(
            Alert.AlertType.CONFIRMATION,
            "Подтвердите операцию");
        confirmWindow.setHeaderText(message);
        confirmWindow.setTitle("Подтверждение операции");
        Optional<ButtonType> result = confirmWindow.showAndWait();
        return result.isPresent() && result.get() == ButtonType.OK;
    }

    public void refreshRemotePickingsButtonAction(ActionEvent actionEvent) {
        refreshRemoteCache();
    }

    private void refreshRemoteCache() {
        try {
            PickingOrderService.clearRemoteCache();
            PickingOrderService.getRemoteCash(remoteOrdersServersCB.getSelectionModel().getSelectedItem());
            remoteOrdersTableView.getItems().clear();
            remoteOrdersTableView.getItems().addAll(PickingOrderService.getRemotePickingOrders());
        } catch (IOException e) {
            logger.error("Error refreshing remote cache", e);
            new Alert(Alert.AlertType.ERROR, "Ошибка обновления списка файлов").showAndWait();
        }
    }
}
