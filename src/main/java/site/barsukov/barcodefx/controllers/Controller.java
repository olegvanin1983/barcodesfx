package site.barsukov.barcodefx.controllers;

import javafx.concurrent.Service;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import site.barsukov.barcodefx.ContextCreator;
import site.barsukov.barcodefx.Validator;
import site.barsukov.barcodefx.model.AboutResource;
import site.barsukov.barcodefx.model.AboutTextEnum;
import site.barsukov.barcodefx.model.PickingServerResource;
import site.barsukov.barcodefx.model.enums.GoodCategory;
import site.barsukov.barcodefx.props.JsonAppProps;
import site.barsukov.barcodefx.props.JsonLastStateProps;
import site.barsukov.barcodefx.props.TabProps;
import site.barsukov.barcodefx.services.*;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Calendar.DECEMBER;
import static java.util.Calendar.JANUARY;
import static site.barsukov.barcodefx.ListFiller.*;
import static site.barsukov.barcodefx.props.JsonCatalogProps.CatalogProp.CATALOG_ENABLED;
import static site.barsukov.barcodefx.props.JsonLastStateProps.LastStateProp.*;
import static site.barsukov.barcodefx.props.JsonPrintProps.PrintProp.USE_PACKAGE_WORK_TYPE;
import static site.barsukov.barcodefx.props.JsonPrintProps.PrintProp.XML_VALIDATION_ENABLED;
import static site.barsukov.barcodefx.props.JsonSystemProps.SystemProp.NEWS_WINDOW_HEIGHT;
import static site.barsukov.barcodefx.props.JsonSystemProps.SystemProp.NEWS_WINDOW_WIDTH;
import static site.barsukov.barcodefx.props.JsonUpdateProps.UpdateProp.CHECK_UPDATES;

public class Controller implements Initializable {
    static final Logger logger = Logger.getLogger(Controller.class);

    public ListView crossborderList;
    public MenuButton chooseFileButton;
    public Label chooseFileLabel;
    public Button scanFileButton;
    public Button ssccButton;
    public ComboBox templatesList;
    public CheckMenuItem updatesMenuItem;
    public ComboBox<GoodCategory> category;
    public TextField vOborotImportFtsINN;
    public TextField vOborotImportFtsDtNum;
    public DatePicker vOborotImportFtsDtDate;
    public ListView withdrawList;
    public TextField markNumberStart;
    public ListView shipmentList;
    public ListView cancellationList;
    public Label comparedFileLabel;
    public TextArea comparedTextArea;
    public CheckBox ignoreCryptoTail;
    public TextField aggregationDocNum;
    public TextField aggregationOrgName;
    public TextField aggregationSccCodeText;
    public TextField aggregationINN;
    public Label fileNameLabel;
    public Button generateXMLButton;
    public Button generateXMLAcceptanceButton;
    public Button generateXMLShipmentButton;
    public TextField height;
    public TextField width;
    public RadioButton isVertical;
    public RadioButton isHorizontal;
    public RadioButton sizeA4;
    public RadioButton sizeTermoPrinter;
    public TextArea userLabelText;
    public CheckBox rangeCheckBox;
    public TextField rangeFrom;
    public TextField rangeTo;
    public TextField userINNText;
    public TextField acceptanceReceiverINN;
    public TextField acceptanceSenderINN;
    public TextField acceptanceDocNum;
    public DatePicker acceptanceDocDate;
    public DatePicker acceptanceDate;
    public ComboBox acceptanceType;
    public TabPane tabPaneElement;
    public CheckBox printMarkNumber;
    public ListView productionList;
    public TextField shipmentId;
    public ImageView logoImage;
    public CheckBox aggregationIpCB;

    private boolean isPackageType = false;
    private String resultFolder = null;
    private java.util.List<Tab> allTabs;
    private WebService webService = new WebService();
    private PropService propService = PropService.getInstance();


    public void chooseFileButtonAction(ActionEvent actionEvent) {
        File csvFile;
        File folder = new File(FilenameUtils.getFullPath(fileNameLabel.getText()));
        if (isPackageType) {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            directoryChooser.setTitle("Выберите папку с кодами маркировки");
            if (folder.exists()) {
                directoryChooser.setInitialDirectory(folder);
            }
            csvFile = directoryChooser.showDialog(new Stage());
        } else {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Выберите файл с кодами маркировки");
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("csv, txt", "*.csv", "*.txt"));
            if (folder.exists()) {
                fileChooser.setInitialDirectory(folder);
            }
            csvFile = fileChooser.showOpenDialog(new Stage());
        }
        if (csvFile != null) {
            fileNameLabel.setText(csvFile.getAbsolutePath());
            resultFolder = csvFile.getParent();
            enableGeneratorButtons();
        }
    }

    public void choosePickingOrderButtonAction(ActionEvent actionEvent) {
        try {
            if (isPackageType) {
                throw new IllegalArgumentException("Функционал не поддерживается в пакетном режиме");
            } else {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/picking_order_list.fxml"));
                Parent root1 = (Parent) fxmlLoader.load();
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setTitle("Пикинг ордера");
                Scene scene = new Scene(root1);
                stage.setScene(scene);
                stage.showAndWait();
                File fileWithCodes = (File) scene.getUserData();
                if (fileWithCodes != null) {
                    fileNameLabel.setText(fileWithCodes.getAbsolutePath());
                    enableGeneratorButtons();
                }
            }
        } catch (Exception e) {
            logger.error("Error opening picking order list", e);
        }
    }

    private void fillData() {
        acceptanceType.setItems(getChangeOptions());
        updatesMenuItem.setSelected(
            propService.getProps().getJsonUpdateProps().getBooleanProp(CHECK_UPDATES));

        category.getSelectionModel().selectedItemProperty().addListener(
            (options, oldValue, newValue) -> {
                hideTabs();
            }
        );
        if (isPackageType) {
            chooseFileButton.setText("Выбрать папку с кодами");
            chooseFileLabel.setText("Выбранная папка:");
            fileNameLabel.setText("Папка не выбрана");
            Alert disabledAlert = new Alert(Alert.AlertType.ERROR, "Функционал недоступен в режиме пакетной обработки");
            ssccButton.setOnAction(new EventHandler() {
                                       @Override
                                       public void handle(Event event) {
                                           disabledAlert.showAndWait();
                                       }
                                   }
            );
            scanFileButton.setOnAction(new EventHandler() {
                                           @Override
                                           public void handle(Event event) {
                                               disabledAlert.showAndWait();
                                           }
                                       }
            );
        }
        fillCategories(category);
        fillProductionList(productionList);
        fillShipmentList(shipmentList);
        fillCancellationList(cancellationList);
        fillWithdrawList(withdrawList);
        fillTemplates(templatesList);
        fillCrossborderList(crossborderList);
    }

    private void enableGeneratorButtons() {
        generateXMLButton.setDisable(false);
        generateXMLShipmentButton.setDisable(false);
        generateXMLAcceptanceButton.setDisable(false);
        tabPaneElement.setDisable(false);
    }

    private List<String> getCsvFileFromFolder(String text) {
        return FileUtils.listFiles(new File(text), new String[]{"csv", "txt"}, true)
            .stream()
            .map(File::getAbsolutePath)
            .collect(Collectors.toList());
    }

    private BaseDocService getPdfCreateService(String csvFileName) {
        if (sizeTermoPrinter.isSelected() && !templatesList.getSelectionModel().isSelected(0)) {
            return new PDFTemplatesCreateService(ContextCreator.createDataMatrixContext(this, csvFileName));
        } else {
            return new PDFCreateService(ContextCreator.createDataMatrixContext(this, csvFileName));
        }
    }

    public void printDatamatrixButtonAction(ActionEvent actionEvent) {
        if (isPackageType) {
            boolean noErrors = true;
            java.util.List<String> csvFiles = getCsvFileFromFolder(fileNameLabel.getText());
            int errorCounter = 0;
            for (String curFile : csvFiles) {
                boolean result = performPackageAction(getPdfCreateService(curFile));
                if (!result) {
                    noErrors = false;
                    errorCounter++;
                }
            }
            if (noErrors) {
                String message = String.format("Файлы обработаны без ошибок. Успешно обработано %d файлов", csvFiles.size());
                new Alert(Alert.AlertType.INFORMATION, message).showAndWait();
            } else {
                String message = String.format("Файлы обработаны c ошибками. Успешно обработано %d файлов из %d. Подробная информация в логах.",
                    csvFiles.size() - errorCounter, csvFiles.size());
                new Alert(Alert.AlertType.ERROR, message).showAndWait();
            }
        } else {
            performAction(getPdfCreateService(fileNameLabel.getText()));
        }
    }

    public void printDatamatrixInPacksAction(ActionEvent actionEvent) throws IOException {
        if (isPackageType) {
            new Alert(Alert.AlertType.ERROR, "Функционал недоступен в режиме пакетной обработки").showAndWait();
        } else {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/print_packs.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("Печать пачками");
            Scene scene = new Scene(root1);
            PrintPacksController controller = fxmlLoader.getController();
            controller.setDataMatrixContext(ContextCreator.createDataMatrixContext(this, fileNameLabel.getText()));
            controller.setUseTemplate(sizeTermoPrinter.isSelected() && !templatesList.getSelectionModel().isSelected(0));
            stage.setScene(scene);
            stage.showAndWait();
            Boolean success = (Boolean) scene.getUserData();
            if (success != null && success) {
                new Alert(Alert.AlertType.INFORMATION, "Файлы успешно созданы. Находятся в той же папке, что и файл с кодами.").showAndWait();
            } else if (success != null && !success) {
                new Alert(Alert.AlertType.ERROR, "При печати произошла ошибка, подробнее в файле логов.").showAndWait();
            }
        }
    }

    public void generateXMLAcceptance(ActionEvent actionEvent) {
        XMLAcceptanceService xmlCreateService = new XMLAcceptanceService(ContextCreator.createAcceptanceContext(this));
        performAction(xmlCreateService);
    }

    public void aggregationSaveButtonAction(ActionEvent actionEvent) {
        XMLAggregationService xmlAggregationService = new XMLAggregationService(ContextCreator.createAggregationContext(this));
        performAction(xmlAggregationService);
    }

    public void vOborotImportFtsButtonAction(ActionEvent actionEvent) {
        XMLVvodVOborotImportFtsService xmlVvodVOborotImportFtsService = new XMLVvodVOborotImportFtsService(ContextCreator.createVvodVOborotImportFtsContext(this));
        performAction(xmlVvodVOborotImportFtsService);
    }

    public void generateXMLWithdrawButtonAction(ActionEvent actionEvent) {
        XMLWithdrawService xmlWithdrawService = new XMLWithdrawService(ContextCreator.createWithdrawContext(this));
        performAction(xmlWithdrawService);
    }

    public void cancellationButtonAction(ActionEvent actionEvent) {
        XMLCancellationService xmlCancellationService = new XMLCancellationService(ContextCreator.createCancellationContext(this));
        performAction(xmlCancellationService);
    }

    public void generateXML(ActionEvent actionEvent) {
        XMLVvodVOborotService xmlCreateService = new XMLVvodVOborotService(ContextCreator.createVvodVOborotContext(this));
        performAction(xmlCreateService);
    }

    public void generateXMLShipment(ActionEvent actionEvent) {
        XMLShipmentService xmlCreateService = new XMLShipmentService(ContextCreator.createShipmentContext(this));
        performAction(xmlCreateService);
    }

    public void generateXMLProduction(ActionEvent actionEvent) {
        XMLProductionService xmlProductionService = new XMLProductionService(ContextCreator.createProductionRFContext(this));
        performAction(xmlProductionService);
    }

    public void vOborotImportCrossborderButtonAction(ActionEvent actionEvent) {
        if (!propService.getProps().getJsonCatalogProps().getBooleanProp(CATALOG_ENABLED)) {
            new Alert(Alert.AlertType.ERROR, "Для формирования файл ввода в оборот при трансграничной торговле " +
                "требуется в настройках подключить каталог товаров").showAndWait();
        } else {
            XMLVvodVOborotCrossborderService xmlCrossborderService = new XMLVvodVOborotCrossborderService(ContextCreator.createVoborotCrossborderContext(this));
            performAction(xmlCrossborderService);
        }
    }

    public void rangeCheckBoxAction(ActionEvent actionEvent) {
        if (rangeCheckBox.isSelected()) {
            rangeFrom.setDisable(false);
            rangeTo.setDisable(false);
        } else {
            rangeFrom.setDisable(true);
            rangeTo.setDisable(true);
        }
    }

    public void scanFileButtonAction(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/scanner.fxml"));
        Parent root1 = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Scanner");
        Scene scene = new Scene(root1);
        stage.setScene(scene);
        stage.showAndWait();
        File scanFile = (File) scene.getUserData();
        if (scanFile != null) {
            fileNameLabel.setText(scanFile.getAbsolutePath());
            enableGeneratorButtons();
        }
    }

    public void termoprinterSelection(ActionEvent actionEvent) {
        if (sizeTermoPrinter.isSelected()) {
            height.setDisable(false);
            width.setDisable(false);
            isHorizontal.setSelected(true);
            isVertical.setSelected(false);
            isHorizontal.setDisable(true);
            isVertical.setDisable(true);
            templatesList.setDisable(false);
        }
    }

    public void sizeA4Selection(ActionEvent actionEvent) {
        if (sizeA4.isSelected()) {
            height.setDisable(true);
            width.setDisable(true);
            isHorizontal.setDisable(false);
            isVertical.setDisable(false);
            templatesList.setDisable(true);
        }
    }

    public void openFortaLink(ActionEvent actionEvent) throws URISyntaxException, IOException {
        Desktop.getDesktop().browse(new URI("https://fourta.org/?utm_source=barcodesfx"));
    }

    public void openTutorial(ActionEvent actionEvent) throws URISyntaxException, IOException {
        Desktop.getDesktop().browse(new URI("https://www.youtube.com/watch?v=sPWce9AgoMc?utm_source=barcodesfx"));
    }

    public void openAbout(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/about.fxml"));
        fxmlLoader.setResources(new AboutResource(AboutTextEnum.ABOUT));
        Parent root1 = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        AboutController controller = fxmlLoader.getController();
        stage.setOnCloseRequest(event -> controller.saveProps());
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("About");
        Scene scene = new Scene(root1);
        stage.setScene(scene);
        stage.showAndWait();
    }

    public void openPickingServersButtonAction(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/picking_servers.fxml"));
        fxmlLoader.setResources(new PickingServerResource(propService));
        Parent root1 = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
//        PickingServersController controller = fxmlLoader.getController();
//        controller.setPropService(propService);
//        stage.setOnCloseRequest(event -> controller.saveProps());
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Servers");
        Scene scene = new Scene(root1);
        stage.setScene(scene);
        stage.showAndWait();
    }

    public void supportLinkAction(ActionEvent actionEvent) throws URISyntaxException, IOException {
        Desktop.getDesktop().browse(new URI("https://money.yandex.ru/to/41001395855445/500"));
    }

    public void comparedButtonAction(ActionEvent actionEvent) {
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Выберите файл с кодами маркировки");
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("csv, txt", "*.csv", "*.txt"));
            File csvFile = fileChooser.showOpenDialog(new Stage());
            comparedFileLabel.setText(csvFile.getAbsolutePath());
            ComparatorService comparatorService = new ComparatorService(ContextCreator.createComparatorContext(this));
            comparedTextArea.setText(comparatorService.getReport());
        } catch (IllegalArgumentException ex) {
            logger.error("Ошибка обработки сравнения: ", ex);
            new Alert(Alert.AlertType.ERROR, ex.getMessage()).showAndWait();
        } catch (Exception e) {
            logger.error("Error comparedButtonAction: ", e);
            new Alert(Alert.AlertType.ERROR, "Unexpected exception: " + e.getMessage()).showAndWait();
        }
    }

    public void ssccButtonAction(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/sscc.fxml"));
        Parent root1 = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("SSCC");
        Scene scene = new Scene(root1);
        stage.setScene(scene);
        stage.showAndWait();
        File scanFile = (File) scene.getUserData();
        if (scanFile != null) {
            fileNameLabel.setText(scanFile.getAbsolutePath());
            enableGeneratorButtons();
        }
    }

    public void ssccAbout(MouseEvent mouseEvent) throws IOException {
        openAboutWindow(AboutTextEnum.SSCC_ABOUT);
    }

    public void openAboutCrossborderAction(MouseEvent mouseEvent) throws IOException {
        openAboutWindow(AboutTextEnum.CROSSBORDER);
    }

    public void nameAbout(MouseEvent mouseEvent) throws IOException {
        openAboutWindow(AboutTextEnum.AGGREGATION_NAME_ABOUT);
    }

    public void withdrawExplanation(MouseEvent mouseEvent) throws IOException {
        openAboutWindow(AboutTextEnum.WITHDRAW_EXPLANATION);
    }

    public void templatesExplanation(MouseEvent mouseEvent) throws IOException {
        openAboutWindow(AboutTextEnum.TEMPLATES_EXPLANATION);
    }

    public void aggregationDocNum(MouseEvent mouseEvent) throws IOException {
        openAboutWindow(AboutTextEnum.AGGREGATION_DOC_NUM);
    }

    private void openAboutWindow(AboutTextEnum textType) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/about.fxml"));
        fxmlLoader.setResources(new AboutResource(textType));
        Parent root1 = (Parent) fxmlLoader.load();

        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("About");
        Scene scene = new Scene(root1);

        stage.setScene(scene);
        stage.showAndWait();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            isPackageType = propService.getProps().getJsonPrintProps().getBooleanProp(USE_PACKAGE_WORK_TYPE);
            allTabs = new ArrayList<>(tabPaneElement.getTabs());
            printMarkNumber.selectedProperty().addListener(
                (options, oldValue, newValue) -> {
                    markNumberStart.setDisable(!printMarkNumber.isSelected());
                }
            );
            hideTabs();
            fillData();
            new Thread(() -> WebService.logAction("START", 0, propService.getProps(), null)).start();
            new Thread(CatalogService.INSTANCE::reloadData).start();

            fillDefaults();
            openNews();
            Service service = new UpdateService(webService);
            service.start();
            changeLogo();
        } catch (Exception e) {
            logger.error("Ошибка при запуске приложения");
            logger.error("Error: ", e);
        }
    }

    private void changeLogo() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        boolean holidayDay = (cal.get(Calendar.DAY_OF_MONTH) >= 25 && cal.get(Calendar.MONTH) == DECEMBER) ||
            (cal.get(Calendar.DAY_OF_MONTH) <= 11 && cal.get(Calendar.MONTH) == JANUARY);
        if (holidayDay) {
            InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("tree.png");
            Image image = new Image(inputStream);
            logoImage.setImage(image);
        }
    }

    private void fillDefaults() {
        JsonLastStateProps lastStateProps = PropService.getInstance().getProps().getJsonLastStateProps();
        GoodCategory goodCategory = GoodCategory.valueOf(lastStateProps.getProp(CATEGORY));
        category.getSelectionModel().select(goodCategory);
        height.setText(lastStateProps.getProp(LABEL_HEIGHT));
        width.setText(lastStateProps.getProp(LABEL_WIDTH));
        userLabelText.setText(lastStateProps.getProp(LABEL_TEXT));
        markNumberStart.setText(lastStateProps.getProp(LABEL_START_NUMBER));
        printMarkNumber.setSelected(lastStateProps.getBooleanProp(LABEL_NUMERATE));
        isVertical.setSelected(lastStateProps.getBooleanProp(LABEL_VERTICAL));
        isHorizontal.setSelected(lastStateProps.getBooleanProp(LABEL_HORIZONTAL));
        if (lastStateProps.getBooleanProp(LABEL_SIZE_A4)) {
            sizeA4.setSelected(true);
            sizeA4.getOnAction().handle(null);
        }
        if (lastStateProps.getBooleanProp(LABEL_SIZE_TERMOPRINTER)) {
            sizeTermoPrinter.setSelected(true);
            sizeTermoPrinter.getOnAction().handle(null);
        }
    }

    private void saveDefaults() {
        JsonAppProps appProps = PropService.getInstance().getProps();

        GoodCategory goodCategory = category.getSelectionModel().getSelectedItem();
        appProps.getJsonLastStateProps().setProp(CATEGORY, goodCategory.name());

        appProps.getJsonLastStateProps().setProp(LABEL_HEIGHT, height.getText());
        appProps.getJsonLastStateProps().setProp(LABEL_WIDTH, width.getText());
        appProps.getJsonLastStateProps().setProp(LABEL_TEXT, userLabelText.getText());
        appProps.getJsonLastStateProps().setProp(LABEL_START_NUMBER, markNumberStart.getText());
        appProps.getJsonLastStateProps().setProp(LABEL_NUMERATE, printMarkNumber.isSelected());
        appProps.getJsonLastStateProps().setProp(LABEL_SIZE_A4, sizeA4.isSelected());
        appProps.getJsonLastStateProps().setProp(LABEL_SIZE_TERMOPRINTER, sizeTermoPrinter.isSelected());
        appProps.getJsonLastStateProps().setProp(LABEL_VERTICAL, isVertical.isSelected());
        appProps.getJsonLastStateProps().setProp(LABEL_HORIZONTAL, isHorizontal.isSelected());

        PropService.getInstance().saveProps(appProps);
    }

    private void hideTabs() {
        TabProps.INSTANCE.updateProperties();
        java.util.List<Tab> visibleTabs = new ArrayList<>();
        for (Tab tab : allTabs) {
            if (TabProps.INSTANCE.isVisible(tab.getId(),
                category.getSelectionModel().getSelectedItem(), isPackageType)) {
                visibleTabs.add(tab);
            }
        }
        tabPaneElement.getTabs().setAll(visibleTabs);
    }


    public void openTabProperties(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/tab_properties.fxml"));

        Parent root1 = (Parent) fxmlLoader.load();
        TabPropertiesController controller = fxmlLoader.getController();
        controller.setTabs(allTabs);
        controller.filData();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Настройка вкладок");
        Scene scene = new Scene(root1);
        stage.setScene(scene);
        stage.showAndWait();
        hideTabs();
    }

    public void openCatalogProperties(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/catalog_properties.fxml"));

        Parent root1 = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Настройка каталога");
        Scene scene = new Scene(root1);
        stage.setScene(scene);
        stage.showAndWait();
    }

    public void validationButtonAction(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/validator.fxml"));

        Parent root1 = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Валидация документа");
        Scene scene = new Scene(root1);
        stage.setScene(scene);
        stage.showAndWait();
    }

    public void openPrintProperties(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/print_properties.fxml"));

        Parent root1 = (Parent) fxmlLoader.load();
        PrintPropertiesController controller = fxmlLoader.getController();
        controller.filData();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Настройка печати");
        Scene scene = new Scene(root1);
        stage.setScene(scene);
        stage.showAndWait();
    }

    public void openSystemProperties(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/system_properties.fxml"));

        Parent root1 = (Parent) fxmlLoader.load();
        SystemPropertiesController controller = fxmlLoader.getController();
        controller.filData();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Системные настройки");
        Scene scene = new Scene(root1);
        stage.setScene(scene);
        stage.showAndWait();
    }

    public void openNews() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/news.fxml"));
        Parent root1 = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Новости");
        Scene scene = new Scene(root1);
        stage.setScene(scene);
        stage.setHeight(propService.getProps().getJsonSystemProps().getFloatProp(NEWS_WINDOW_HEIGHT));
        stage.setWidth(propService.getProps().getJsonSystemProps().getFloatProp(NEWS_WINDOW_WIDTH));
        stage.showAndWait();
    }

    public void exportPropertiesAction(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Сохранить настройки");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Properties (*.json)", "*.json"));
        fileChooser.setInitialFileName("*.json");
        Stage stage = (Stage) fileNameLabel.getScene().getWindow();
        File file = fileChooser.showSaveDialog(stage);
        if (file != null) {
            if (!file.getName().contains(".")) {
                file = new File(file.getAbsolutePath() + ".json");
            }
            propService.saveProps(propService.getProps(), file);
        }
    }

    public void importPropertiesAction(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Выберите файл с сохраненными настройками");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("json", "*.json"));
        File file = fileChooser.showOpenDialog(new Stage());
        if (file != null) {
            propService.saveProps(propService.getProps(file));
        }
    }

    public void enableUpdateAction(ActionEvent actionEvent) {
        JsonAppProps allProps = propService.getProps();
        allProps.getJsonUpdateProps().setProp(CHECK_UPDATES, updatesMenuItem.isSelected());
        propService.saveProps(allProps);
    }

    public void onCloseActions() {
        saveDefaults();
    }

    private void performAction(BaseDocService service) {
        try {
            if (isPickingOrder(service.getContext().getCsvFileName())) {
                DirectoryChooser directoryChooser = new DirectoryChooser();
                directoryChooser.setTitle("Выберите папку для сохранения");

                File folder = directoryChooser.showDialog(new Stage());
                if (folder != null) {
                    service.getContext().setResultFolder(folder.getAbsolutePath());
                } else {
                    return;
                }
            }
            boolean validationEnabled = propService.getProps().getJsonPrintProps().getBooleanProp(XML_VALIDATION_ENABLED);
            File xmlFile = service.generateResult();
            boolean fileIsValid = true;
            if (service.isXmlResult() && validationEnabled) {
                fileIsValid = Validator.isXmlValid(xmlFile);
            }
            if (fileIsValid) {
                new Alert(Alert.AlertType.INFORMATION, "Файл сохранен в " + xmlFile.getAbsolutePath()).showAndWait();
            } else {
                String message = String.format("Файл сохранен в %s. %nСгенерированный файл не соответствует xsd схеме. %nВоспользуйтесь функцией валидации для получения дополнительной информации.",
                    xmlFile.getAbsolutePath());
                new Alert(Alert.AlertType.WARNING, message).showAndWait();
            }

        } catch (IllegalArgumentException ex) {
            logger.error("Ошибка обработки документа: ", ex);
            new Alert(Alert.AlertType.ERROR, ex.getMessage()).showAndWait();
        } catch (Exception e) {
            String message = "Error in : " + service.getServiceName();
            logger.error(message, e);
            new Alert(Alert.AlertType.ERROR, "Unexpected exception: " + e.getMessage()).showAndWait();
        }
    }

    private boolean isPickingOrder(String csvFileName) {
        return PickingOrderService.isPickingOrder(csvFileName);
    }

    private boolean performPackageAction(BaseDocService service) {
        try {
            service.generateResult();
            return true;
        } catch (IllegalArgumentException ex) {
            logger.error("Ошибка обработки документа: ", ex);
            return false;
        } catch (Exception e) {
            String message = "Error in : " + service.getServiceName();
            logger.error(message, e);
            return false;
        }

    }

    public String getResultFolder() {
        return resultFolder;
    }

    public void setResultFolder(String resultFolder) {
        this.resultFolder = resultFolder;
    }
}
