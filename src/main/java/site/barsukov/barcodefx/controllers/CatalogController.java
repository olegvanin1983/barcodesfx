package site.barsukov.barcodefx.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import site.barsukov.barcodefx.props.JsonAppProps;
import site.barsukov.barcodefx.props.JsonCatalogProps;
import site.barsukov.barcodefx.services.CatalogService;
import site.barsukov.barcodefx.services.PropService;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import static site.barsukov.barcodefx.props.JsonCatalogProps.CatalogProp.CATALOG_ENABLED;
import static site.barsukov.barcodefx.props.JsonCatalogProps.CatalogProp.FILE_PATH;

public class CatalogController implements Initializable {
    public Label fileLabel;
    public CheckBox useCatalog;

    private PropService propService = PropService.getInstance();
    private JsonCatalogProps catalogProps;
    private JsonAppProps appProps;

    public void chooseFileButtonAction(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Выберите файл выгрузки ГС1");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Файл выгрузки", "*.xlsx"));
        File csvFile = fileChooser.showOpenDialog(new Stage());
        fileLabel.setText(csvFile.getAbsolutePath());
    }

    public void okButtonAction(ActionEvent actionEvent) {
        catalogProps.setProp(FILE_PATH, fileLabel.getText());
        catalogProps.setProp(CATALOG_ENABLED, useCatalog.isSelected());
        propService.saveProps(appProps);
        new Thread(CatalogService.INSTANCE::reloadData).start();
        Stage stage = (Stage) fileLabel.getScene().getWindow();
        stage.close();
    }

    public void cancelButtonAction(ActionEvent actionEvent) {
        Stage stage = (Stage) fileLabel.getScene().getWindow();
        stage.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        appProps = propService.getProps();
        catalogProps = appProps.getJsonCatalogProps();
        useCatalog.setSelected(catalogProps.getBooleanProp(CATALOG_ENABLED));
        fileLabel.setText(catalogProps.getProp(FILE_PATH));
    }
}
